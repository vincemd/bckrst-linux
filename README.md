# Backup Debian with Nextcloud
---

```sh
bash <(wget -qO- https://gitlab.com/vincemd/backup-w-nextcloud/-/raw/main/script.sh)
```
---
This script allows you to backup or restore a workstation with nextcloud.
This script works on the GNOME desktop environment on Debian based destibution.

Tested on Ubuntu and Debian

Are saved :
- The list of applications (apt)
- The GNOME desktop environment
- The list of Flatpak

The script offers in addition:
- Adding contrib and non-free repositories
- The flatpak repositories
- Podman registries
- Creation of new SSH keys
- An automatic backup every sunday evening to the nextcloud

Where are the files saved?
- The files are saved on /home/$USER/Nextcloud/.backup_env_linux/

---

Ce script permet de sauvegarder ou restaurer un poste de travail avec nextcloud.
Ce script fonctionne sur l'environnement de bureau GNOME sur les destibution basé sur Debian.

Testé sur Ubuntu et Debian

Sont sauvegardés :
- La liste des applications (apt)
- L'environnement de bureau GNOME
- La liste des Flatpak

Le script propose en plus:
- Ajout des repository contrib et non-free
- Les flatpak repositories
- Les Podman registries
- Création de nouvelle clef SSH
- Une sauvegarde automatique les dimanche soir vers le nextcloud

Ou sont sauvegardé les fichiers ?
- Les fichier sont sauvegardé sur /home/$USER/Nextcloud/.backup_env_linux/


## License

Copyright 2023 ¨Backup Debian with Nextcloud¨
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
