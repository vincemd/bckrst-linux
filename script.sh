#!/bin/bash

######################################################
# Name of script: Backup Debian with Nextcloud       #
# Utilite: Backup/Restore Debian env with Nextcloud  #
# Autor: Vinc Md | dev@vincentmodere.net             #
# Version: V1.2 | 20220810                           #
# Licence: MIT                                       #
# Git: https://gitlab.com/vincemd/bckrst-linux       #
######################################################


#-----------------------------------------------------------------------------#
#                                  Variables                                  #
#-----------------------------------------------------------------------------#

# variables environment

nextcloudfolder="/home/$USER/Nextcloud/.backup_env_linux"
homefolder="/home/$USER"
aptfile="apt-packages.list"
dconffile="dconf-gnome.sav"
checknextcloud="/bin/nextcloud"
flatpakfile="flatpak.list"

# Color the script

red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
reset=`tput sgr0`

#-----------------------------------------------------------------------------#
#                                  Fonctions                                  #
#-----------------------------------------------------------------------------#

#------------#
# Check sudo #
#------------#
function checksudo
{
  sudo -l -U $USER
        if [ $? = 0 ]
            then
            echo  "${green}Nice, $USER is sudoer${reset}"
        else
            echo  "${red}User is not sudoer${reset}"
            echo  "${yellow}Add $USER sudoer by root user${reset}"
            echo  "${yellow}In root mode enter ( sudo adduser $USER sudo )${reset}"
            echo  "${yellow}Reboot and reload script${reset}"
            read -s -n 1 -p "Press any key to continue . . ."
            echo ""
            su root
            exit
        fi

}

#---------------#
# Check distrib #
#---------------#
function distrib
{
  echo "${yellow}Check distib Linux${reset}"
    distrib=$(grep -E 'Debian | Ubuntu' /etc/*-release)
        if [ $? = 0 ]
            then
            echo  "${green}Distib is Debian or Ubuntu${reset}"
        else
            echo  "${red}!!!! Distib is not Debian or Ubuntu !!!!${reset}"
            echo  "${red}This Script is not tested on this distib${reset}"
        fi
}

#-----------------#
# Check Nextcloud #
#-----------------#
function nextcloud
{
  echo "Check is nextcloud is instaled"
  checknextcloud=$(which nextcloud)
        if [ $? = 0 ]
            then
            echo  "${green}Nextcloud is instaled ${reset}"
        else
            echo  "${red}! Nextcloud is not instaled !${reset}"
            apt install nextcloud-desktop -y
            echo  "${green}Nextcloud is instaled ${reset}"
        fi
}


#---------------------#
# Check files restore #
#---------------------#
function restorecheck
{
    echo "${yellow}Restore Distrib${reset}"
    echo "${yellow}Have you install and sync Nextcloud on this device ? ${reset}"
    echo "${yellow}Check if files is present in $nextcloudfolder ${reset}"
    read -s -n 1 -p "Press any key to continue . . ."
    echo
    FILE=$nextcloudfolder/$aptfile
        if [ -f "$FILE" ];
                    then
            echo  "${green} $nextcloudfolder/$aptfile is present ${reset}"
                    else
            echo  "${red} $nextcloudfolder/$aptfile not found${reset}"
            read -s -n 1 -p "Press any key to continue . . ."
        fi
    FILE=$nextcloudfolder/$dconffile
        if [ -f "$FILE" ];
                    then
            echo  "${green} $nextcloudfolder/$dconffile is present ${reset}"
                    else
            echo  "${red} $nextcloudfolder/$dconffile not found${reset}"
            read -s -n 1 -p "Press any key to continue . . ."
        fi
    FILE=$nextcloudfolder/$flatpakfile
        if [ -f "$FILE" ];
                    then
            echo  "${green} $nextcloudfolder/$flatpakfile is present ${reset}"
                    else
            echo  "${red} $nextcloudfolder/$flatpakfile not found${reset}"
            read -s -n 1 -p "Press any key to continue . . ."
        fi
}

#----------------#
# Update distrib #
#----------------#
function aptupdate
{
  echo "Update de la distrib Linux"
    aptupdate=$(apt update 2>&1)
    echo -e "$aptupdatem"
    aptupdateresult=$(echo "$aptupdate" | grep "Failed")
        if [ $? = 0 ]
            then
            echo  "${red}You are not connected on Internet${reset}"
        else
            echo  "${green}APT update source is OK${reset}"
        fi
    apt full-upgrade -y
    aptfullupgrade=$?
    echo $aptfullupgrade
        if [ $? = 0 ]
            then
            echo  "${green}Debian is to Update${reset}"
        else
            echo  "${red}! Debian is not Update !${reset}"
        fi
    while true; do
    read -p "${yellow}Do you wish to add contrib and non-free repro ? (yes/no)${reset}" yn
    case $yn in
        [Yy]* )
            echo "${yellow}adding contrib and non-free repro${reset}"
            apt install software-properties-common -y
            add-apt-repository contrib
            add-apt-repository non-free
            apt dist-upgrade -y
            break;;
        [Nn]* )
            echo "${yellow}No add contrib and non-free repro${reset}"
            break;;
        * )
            echo "Please answer yes or no.";;
    esac
done
}

#------------------#
# Backup dpkg list #
#------------------#
function backupdpkglist
{
  echo "Backup DPKG List"
  backupdpkglist=$(dpkg --get-selections > $nextcloudfolder/$aptfile)
        if [ $? = 0 ]
            then
            echo  "${green}Backup OK DPKG List in $nextcloudfolder/$aptfile ${reset}"
        else
            echo  "${red}! Backup KO DPKG List !${reset}"
        fi
}

#----------------------------#
# Backup Gnome configuration #
#----------------------------#
function backupdconf
{
  echo "Backup Gnome Shell environment"
   backupdconf=$(dconf dump / > $nextcloudfolder/$dconffile)
        if [ $? = 0 ]
            then
            echo  "${green}Backup OK Gnome Shell environment in $nextcloudfolder/$dconffile ${reset}"
        else
            echo  "${red}! Backup KO Gnome Shell environment !${reset}"
        fi
}

#---------------------#
# Backup Flatpak list #
#---------------------#
function backupflatpak
{
  echo "Backup Flatpak List"
   backupflatpak=$(flatpak list --columns=application --app > $nextcloudfolder/$flatpakfile)
        if [ $? = 0 ]
            then
            echo  "${green}Backup OK Flatpak List in $nextcloudfolder/$flatpakfile ${reset}"
        else
            echo  "${red}! Backup KO Flatpak List !${reset}"
        fi
}

#-------------------#
# Restore dpkg list #
#-------------------#
function restoredpkglist
{
        while true; do
    read -p "${yellow}Do you wish to Restore apt list with dpkg selections ? (yes/no)${reset}" yn
    case $yn in
        [Yy]* )
            echo "${yellow}Restore apt list ${reset}"
            lsuser=$(ls /home/)
            echo -e "$lsuser"
              apt-get update
              apt-get install dselect
              dselect update
              restoredpkglist=$(dpkg --set-selections < /home/$lsuser/Nextcloud/.backup_env_linux/apt-packages.list)
              apt-get -u dselect-upgrade
        if [ $? = 0 ]
            then
            echo  "${green}Restore OK APT List${reset}"
        else
            echo  "${red}! Restore KO APT List !${reset}"
        fi
            break;;
        [Nn]* )
            echo "${yellow}No restore APT List${reset}"
            break;;
        * )
            echo "Please answer yes or no.";;
    esac
done
  echo "Backup APT List"
}

#-----------------------------#
# Restore Gnome configuration #
#-----------------------------#
function restoredconf
{
    while true; do
    read -p "${yellow}Do you wish to Restore Gnome Shell environment for $USER ? (yes/no)${reset}" yn
    case $yn in
        [Yy]* )
            echo "${yellow}Restore Gnome Shell environment for $USER ${reset}"
            restoredconf=$(dconf load / < $nextcloudfolder/$dconffile)
        if [ $? = 0 ]
            then
            echo  "${green}Restore OK Gnome Shell environment${reset}"
        else
            echo  "${red}! Restore KO Gnome Shell environment !${reset}"
        fi
            break;;
        [Nn]* )
            echo "${yellow}No restore Gnome Shell Env${reset}"
            break;;
        * )
            echo "Please answer yes or no.";;
    esac
done
}

#--------------------#
# Add flatrpak repro #
#--------------------#
function flatrpakrepro
{
    while true; do
    read -p "${yellow}Do you wish to add flatrpak repro ? (yes/no)${reset}" yn
    case $yn in
        [Yy]* )
            echo "${yellow}adding flatrpak repro${reset}"
            flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
            break;;
        [Nn]* )
            echo "${yellow}No add flatrpak repro${reset}"
            break;;
        * )
            echo "Please answer yes or no.";;
    esac
done
}

#----------------------#
# Restore Flatpak list #
#----------------------#
function restoredflatpack
{
    while true; do
    read -p "${yellow}Do you wish to Restore Flatpak list ? (yes/no)${reset}" yn
    case $yn in
        [Yy]* )
            echo "${yellow}Restore Flatpak list${reset}"
            echo "${yellow}this may take a while${reset}"
            restoredconf=$(flatpak install -y $(< $nextcloudfolder/$flatpakfile) )
        if [ $? = 0 ]
            then
            echo  "${green}Restore OK Flatpak list${reset}"
        else
            echo  "${red}! Restore KO Flatpak list !${reset}"
        fi
            break;;
        [Nn]* )
            echo "${yellow}No restore Flatpak list${reset}"
            break;;
        * )
            echo "Please answer yes or no.";;
    esac
done
}

#-----------------------#
# Add Podman registries #
#-----------------------#
function podmanrepro
{
    while true; do
    read -p "${yellow}Do you wish to add Podman registries ? (yes/no)${reset}" yn
    case $yn in
        [Yy]* )
            podmanrepro=$(cat /etc/containers/registries.conf | grep docker.io)
        if [ $? = 0 ]
            then
            echo "${green}Podman registries already present${reset}"
        else
            echo  "${yellow}Adding Podman registries${reset}"
            echo "unqualified-search-registries = ['registry.fedoraproject.org', 'registry.access.redhat.com', 'registry.centos.org', 'docker.io']" >> /etc/containers/registries.conf
        fi
            break;;
        [Nn]* )
            echo "${yellow}No add Podman registries${reset}"
            break;;
        * )
            echo "Please answer yes or no.";;
    esac
done
}

#----------------------#
# Generate new SSH key #
#----------------------#
function newsshkey
{
    while true; do
    read -p "${yellow}Do you wish to gnerate new SSH key ? (yes/no)${reset}" yn
    case $yn in
        [Yy]* )
            echo "${yellow}New SSh key${reset}"
            ssh-keygen -t rsa -b 4096
            ssh-keygen -t ed25519
            echo "${yellow}Show new SSh key (RSA)${reset}"
            cat ~/.ssh/id_rsa.pub
            echo "${yellow}Show new SSh key (ed25519)${reset}"
            cat ~/.ssh/id_ed25519.pub
            read -s -n 1 -p "${yellow}Press any key to continue . . .${reset}"
            echo ""
            break;;
        [Nn]* )
            echo "${yellow}No new SSh key${reset}"
            break;;
        * )
            echo "Please answer yes or no.";;
    esac
done
}

#--------------------#
# Backup Script cron #
#--------------------#
function bckscriptcron
{
    while true; do
    read -p "${yellow}Do you wish to automated backup ? (yes/no)${reset}" yn
    case $yn in
        [Yy]* )
        bckcron=/var/scripts/backupaptdoncf.sh
        if [ -f "$bckcron" ]
            then
            echo "${green}Automated backup registries already present${reset}"
        else
            echo "${yellow}Create automated backup${reset}"
            mkdir /var/scripts
            lsuser=$(ls /home/)
            echo -e "$lsuser"
            touch /var/scripts/backupaptdoncf.sh
            cat <<EOF > /var/scripts/backupaptdoncf.sh
#!/bin/bash

################################################
# Name of script : bckrst-linux                #
# Utilite : Backup APT DCONF in Nextcloud      #
################################################

dpkg --get-selections > /home/$lsuser/Nextcloud/.backup_env_linux/apt-packages.list
dconf dump / > /home/$lsuser/Nextcloud/.backup_env_linux/dconf-gnome.sav 
flatpak list --columns=application --app > /home/$lsuser/Nextcloud/.backup_env_linux/flatpak.list
EOF
            chmod +x /var/scripts/backupaptdoncf.sh
            echo "0 23 * * 7 $lsuser /var/scripts/backupaptdoncf.sh" > /etc/cron.d/backupaptdoncf
            echo "${green}Automated backup is create${reset}"
        fi
            break;;
        [Nn]* )
            echo "${yellow}No automated backup${reset}"
            break;;
        * )
            echo "Please answer yes or no.";;
    esac
done
}


#--------------------------------------------------------------------------#
#                                  Script                                  #
#--------------------------------------------------------------------------#

function scriptbackup
{
  echo "${yellow}Backup Distrib in $nextcloudfolder ${reset}"
      distrib
      backupdpkglist
      backupdconf
      backupflatpak
}

function scriptrestore
{
  echo "${yellow}Restore Distrib${reset}"
      checksudo
      distrib
      sudo bash -c "$(declare -f nextcloud); nextcloud"
      restorecheck
      echo "${yellow}You need to be sudoer${reset}"
      sudo bash -c "$(declare -f aptupdate); aptupdate"
      sudo bash -c "$(declare -f restoredpkglist); restoredpkglist"
      restoredconf
      flatrpakrepro
      restoredflatpack
      sudo bash -c "$(declare -f podmanrepro); podmanrepro"
      newsshkey
      sudo bash -c "$(declare -f bckscriptcron); bckscriptcron"
}

#--------------------------------------------------------------------------#
#                                 Options                                  #
#--------------------------------------------------------------------------#

while getopts "rb" option; do
   case $option in
      b)
# bacukup procces
      scriptbackup
         exit;;
      r)
# restore  procces
      scriptrestore
         exit;;
     *)
            echo "${red}Error: Invalid option${reset}"
            echo "${yellow}Execute script with -b for backup or -r for restore${reset}"
         exit;;
   esac
done

#--------------------------------------------------------------------------#
#                                   Menu                                   #
#--------------------------------------------------------------------------#

while true; do
  echo "+---------------------------------+"
  echo "| Restore / Backup with Nextcloud |"
  echo "+---------------------------------+"
  echo ""
  echo ""
  echo "Choose an option :"
  echo ""
  echo "1. Run a backup to nextcloud"
  echo "2. Restore a backup on nextcloud"
  echo "3. Exit"

  read option

  if [ $option -eq 1 ]; then
# bacukup procces
    scriptbackup
  elif [ $option -eq 2 ]; then
# restore  procces
    scriptrestore
  elif [ $option -eq 3 ]; then
    break
  else
    echo "${red}Error: Invalid option${reset}"
  fi
done
